/**
 * Simplifed reactive Javascript store.
 */

'use strict';

class StateStore
{
    _hasKey(key)
    {
        return Object.prototype.hasOwnProperty.call(this.store, key);
    }

    constructor() {
        this.store = [];
    }

    /**
     * Return the current payload of the given key.
     * If not key given, returns the payload of all the keys.
     * @param key: The key to look for in the store.
     */
    getState(key)
    {
        if (key) {
            if (this._hasKey(key))
            {
                return this.store[key].payload;
            }
            else
            {
                return undefined;
            }
        }
        let kvs = {};
        Object.keys(this.store).forEach(x => kvs[x] = this.store[x].payload);
        return kvs;
    }

    /**
     * Register a new key in the store (does nothing if it already exist).
     * @param key: The key to add.
     * @param value: if given, store this value for this key (only if the key doesn't yet exist).
     */
    register(key, value)
    {
        if (!this._hasKey(key))
        {
            this.store[key] = {
                followers: {},
                payload: value
            };
        }
    }

    /**
     * Subscribe to changes of a key lying in the store.
     * @param key: The key in the store we're looking for.
     * @param callback: A function to be called whenever a change occurs to the given key.
     */
    subscribe(key, callback)
    {
        if (!this._hasKey(key))
        {
            this.store[key] = {
                followers: {},
                payload: undefined
            };
        }
        let idx = globalThis.crypto.randomUUID();
        this.store[key].followers[idx] = callback;
        return {
            unsubscribe: () => {
                delete this.store[key].followers[idx];
            }
        }
    }

    /**
     * Call the given function for the given key, then subscribe for future changes.
     * @param key: The key in the store we're looking for.
     * @param callback: A function to be called now and whenever a change occurs to the given key.
     */
    trigger(key, callback)
    {
        callback(key, undefined, this.getState(key));
        return this.subscribe(key, callback);
    }

    /**
     * Change the current value associated to the given key, then call all the callbacks if any.
     * @param key: the key to update its value (create if doesn't exist yet).
     * @param value: The new value to store.
     * @param create: create if doesn't exist yet and if this attribute is true.
     */
    dispatch(key, value, create)
    {
        if (create)
        {
            this.register(key);
        }
        if (this._hasKey(key))
        {
            let oldVal = this.getState(key);
            this.store[key].payload = value;
            for (let cb of Object.values(this.store[key].followers))
            {
                cb(key, oldVal, value);
            }
        }
    }
}

const store = new StateStore();

export { store, StateStore };
