/**
 * backend.mjs
 * Currently uber-simple, this module will ease both http and ws connections.
 */

/**
 * Fetch static files from the backend as text file.
 * @aram {String} path - of the data to fetch
 * @return {String} The data found as a String
 */
// eslint-disable-next-line func-style
async function fetchStaticAsString(path)
{
    // eslint-disable-next-line no-return-await
    return await (await globalThis.fetch(path)).text();
}

export { fetchStaticAsString };
