import { StateStore, store } from './state-store.mjs';
import { CustomElementIf } from './custom-element-if.mjs';
import { fetchStaticAsString } from './backend.mjs';
import { route } from './router.mjs';

export {
    StateStore,
    store,
    CustomElementIf,
    fetchStaticAsString,
    route
};
