/**
 * Get current value of the url and give a state to be followed from other
 * componenents in the web page in order to modify it.
 */

import { StateStore } from './state-store.mjs';

const ROUTE_STORE_KEY = "route";
if (-1 == globalThis.window.location.href.indexOf('#'))
{
    globalThis.window.location.href += '#/home';
}


class Router
{
    constructor()
    {
        this._route = new StateStore();
        let loc = globalThis.window.location.href;
        this._route.register(
            ROUTE_STORE_KEY,
            loc.substr(loc.indexOf('#') + 2)
        );
    }

    /**
     * Get the current route.
     * @return the current route.
     */
    currentValue()
    {
        return this._route.getState(ROUTE_STORE_KEY);
    }

    /**
     * run given callback to the current route value, then keep it as subscriber.
     * @param callback to call with the current route value.
     */
    subscribe(callback)
    {
        let cb = (k, oldVal, newVal) => {
            return callback(oldVal, newVal);
        };
        return this._route.trigger(ROUTE_STORE_KEY, cb);
    }

    /**
     * Update the route value.
     * @param newRoute to store in the router.
     */
    update(newRoute)
    {
        const newVal = newRoute.substr(newRoute.indexOf('#') + 2);
        this._route.dispatch(ROUTE_STORE_KEY, newVal);
        let loc = globalThis.window.location.href;
        loc = loc.substr(0, loc.indexOf('#') + 2);
        globalThis.window.location.href = loc + newRoute;
    }

    /**
     * Build an HTML link which's able to update the route.
     * @param href: The new route when the link is clicked.
     * @param text: The link text.
     * @return the HTML link to be put somewhere in the page.
     */
    buildLink(href, text)
    {
        let link = globalThis.document.createElement('a');
        link.href = href;
        // TODO(JBR) we should protect against injection here.
        link.innerHTML = text;
        link.addEventListener("click", () => {
            this.update(href);
        });
        return link;
    }
}

const route = new Router();

export { route };
