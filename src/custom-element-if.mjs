/**
 * custom-element-if.mjs
 * Interface the base for Faeries' like custom elements.
 * This will be the masterpiece of this library.
 */

/* Local references */
import { fetchStaticAsString } from './backend.mjs';
import { route } from './router.mjs';
import { store, StateStore } from './state-store.mjs';

class CustomElementIf extends globalThis.HTMLElement
{
    /** Public API - To be overriden */

    /**
     * Define here where do we get the custom elements data in your backend.
     */
    static customElementDir = 'custom-elements';

    /**
     * Follow the updates on the custom element's attributes.
     * The object expects the name of the attribute as a key, and a callback
     * accepting the attribute value as parameter.
     */
    static defaultAttributes = { };

    /**
     * Name of the tag to be registered, or internally used in the Sidh's
     * custom elements store.
     */
    static tagName = undefined;

    /**
     * Method called at the end of the constructor.
     */
    postConstruction() { }

    /**
     * Called when the content of the custom element is ready.
     * Use it to render dynamic content from attributes, for instance.
     */
    render() { }

    /**
     * A good idea wiykd be to extend this constructor to add new custom
     * states to the internal store.
     */
    constructor()
    {
        super();
        // Internal store to track any changes to the current instance of this
        // element.
        console.log('Construct element');
        this._internalStore = new StateStore();
        this._subscriptions = [];
        let attributesName = Object.keys(this.constructor.defaultAttributes);
        Object.keys(this.constructor.defaultAttributes).forEach(attribute => {
            this._internalStore.register(attribute, null);
            this._subscriptions.push(this._internalStore.trigger(
                attribute,
                this.constructor.defaultAttributes[attribute]
            ));
        });
        // For documentation: 'content-load' is a special state to tells the
        // custom element html/css data has been loaded from backend.
        this._internalStore.register('content-load', false);
        // Render the content of this element as soon as the content has been
        // loaded from backend.
        this._subscriptions.push(this._internalStore.trigger(
            'content-load',
            (key, oldValue, currentValue) => {
                if (currentValue == true) this.render();
            }
        ));
        this.loadInnerHTML().then((data) => {
            this.innerHTML = data;
            this._internalStore.dispatch('content-load', true);
        });
        // For documentation: 'attribute-update' is triggered whenever an
        // attribute has been changed. Its value will be an object with three
        // keys: name, old, current.
        this._internalStore.register('attribute-change', null);
        this.postConstruction();
    }

    /** Private API - Do not override */

    /**
     * Load the inner html data from static files.
     */
    async loadInnerHTML()  // Cannot override real private method (# prefixed)
    {
        let tagName = this.constructor.tagName;
        let backendBaseDir = this.constructor.customElementDir;
        let remoteElemDir = `{backendBaseDir}/${tagName}`;
        let cssData = await fetchStaticAsString(`${remoteElemDir}/style.css`);
        let htmlData = await fetchStaticAsString(`${remoteElemDir}/index.html`);
        return `<style type="text/css" scoped>
    ${cssData}
</style>
${htmlData}`;
    }

    /** Original HTMLElement API - Do not override */

    /**
     * Apply changes whenever the attribute attr is updated.
     * @param attr {String} the attribute which has changed.
     * @param oldValue {String} the old value.
     * @param newValue {String} the new value.
     */
    attributeChangedCallback(attr, oldValue, newValue)
    {
        this._internalStore.dispatch(attr, newValue);
        this._internalStore.dispatch('attribute-change', {
            'name': attr,
            'old': oldValue,
            'current': newValue
        });
    }

    /**
     * Method called whenevere the custom element is instanciated.
     * Here we connect the attributes of the current instance to the content:
     * We check if the attribute has been given, then dispatch its value.
     */
    connectedCallback()
    {
        Object.keys(this.defaultAttributes).forEach((attr) => {
            let htmlAttr = this.getAttribute(attr);
            if (htmlAttr)
            {
                this._internalStore.dispatch(attr, htmlAttr);
                this._internalStore.dispatch('attribute-change', {
                    'name': attr,
                    'current': htmlAttr
                });
            }
        });
    }

    /**
     * Method called whenever the current instance is destroyed.
     */
    disconnectedCallBack() {
        this._subscriptions.forEach((sub) => { if (sub) sub.unsubscribe(); });
    }

    /**
     * Attributes of the current custom element.
     */
    static get observedAttributes() {
        return Object.keys(this.defaultAttributes);
    }
}

export { CustomElementIf };
