/**
 * router.test.js
 */

import { route } from '../src/router.mjs';


test('get default route value', () => {
    expect(route.currentValue()).toBe('home');
});

test('subscribe then update route', () => {
    let sub = route.subscribe((o, n) => {
        if (o)
        {
            expect(n).toBe('test');
        }
        else
        {
            expect(n).toBe('home');
        }
    });
    expect(sub.hasOwnProperty('unsubscribe')).toBe(true);
    route.update('#/test');
});

test('update then check current value', () => {
    route.update('#/test');
    expect(route.currentValue()).toBe('test');
});

test('build link', () => {
    let ref = "#/test";
    let link = route.buildLink(ref, 'test me');
    expect(link.href.substr(link.href.length - ref.length)).toBe('#/test');
    expect(link.text).toBe('test me');
    link.click();
    expect(route.currentValue()).toBe('test');
});
