/**
 * state-store.test.mjs
 */

import { StateStore } from '../src/state-store.mjs';


test('stores an retreive dark-mode boolean value', () => {
    const store = new StateStore();
    store.register('dark-mode', '');
    let darkModeEnabled = true;
    store.subscribe(
        'dark-mode',
        (key, oldVal, newVal) => {
            darkModeEnabled = newVal;
        });
    store.dispatch('dark-mode', false);
    let states = store.getState();
    expect(Object.keys(states).length).toBe(1);
    expect(states['dark-mode']).toBe(false);
    expect(darkModeEnabled).toBe(false);
});

test('multiple subscriptions', () => {
    const store = new StateStore();
    store.register('answer', 42);
    let subs = [];
    for (let i = 0; i < 5; i++)
    {
        subs.push(store.subscribe(
            'answer',
            (key, o, n) => {
                // We shouldn't call the subscription in this test.
                expect(true).toBe(false);
            })
        );
    }
    expect(subs.length).toBe(5);
    for (let sub of subs)
    {
        expect(sub.hasOwnProperty('unsubscribe')).toBe(true);
        sub.unsubscribe();
    }
    store.dispatch('answer', 21);  // Shouldn't trigger expect true to be false.
});
