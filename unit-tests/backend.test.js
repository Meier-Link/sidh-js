/*
 * backend.test.js
 */

import { fetchStaticAsString } from '../src/backend.mjs';

test('get some static file', async () => {
    const testAgainst = "<!doctype html>";
    const content = await fetchStaticAsString('https://www.google.com');
    expect(content.substr(0,testAgainst.length)).toBe(testAgainst);
});
