/*
 * custom-element-if.test.js
 */

import { CustomElementIf } from '../src/custom-element-if.mjs';


const sleep = (ms) => {
    return new Promise(resolve => setTimeout(resolve, ms));
}


class TestElement extends CustomElementIf
{
    static defaultAttributes = {
        'test-attribute': (key, oldVal, currentVal) => {
            this.testAttribute = currentVal;
        }
    };

    postConstruction()
    {
        this.testAttribute = null;
    }

    /* overriden for test purpose */
    async loadInnerHTML()
    {
        await sleep(1);
        return '';
    }
}


test('dispatch elements events', () => {
    let elem = new TestElement();
    let states = Object.keys(elem._internalStore.getState());
    expect(states.indexOf('content-load') >= 0).toBe(true);
    expect(elem._internalStore.getState('content-load')).toBe(false);
    expect(states.indexOf('attribute-change') >= 0).toBe(true);
    expect(elem._internalStore.getState('attribute-change')).toBe(null);
});
