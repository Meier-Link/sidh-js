/**
 * mocks/client.js
 * @brief Provide a mock to what we expect from the browser in this module.
 */

import crypto from 'crypto';
import { JSDOM } from 'jsdom';
import { Element } from 'html-element';
import fetch from 'node-fetch';


globalThis.crypto = crypto;

const dom = new JSDOM();
globalThis.document = dom.window.document;
globalThis.window = dom.window;

globalThis.HTMLElement = Element;

if (!globalThis.fetch)
{
    // See https://github.com/node-fetch/node-fetch#providing-global-access
    globalThis.fetch = fetch;
}
