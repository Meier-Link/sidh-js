export default {
    input: 'src/main.js',
    output: [
        {
            file: 'build/sidh.js',
            format: 'umd',
            name: 'sidh'
        }
    ]
};
