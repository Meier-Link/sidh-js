# sidh-js

JS framework without dependencies which provides basics to ease web components development.

## Requirements

### For use in your project

Nothing.

### To develop on this library

* eslint (^7.12.1)
* jest (^26.6.3)
* jsdom (^16.7.0)
* node-fetch (3.0)
* rollup (^2.60.2)
* html-element (^2.3.1)

Install those dependencies through `npm install`.

## Installation

Currently, import this repository in your project's static path, then import
from the main.js file.

## Contributing

Contributions are always welcome :)

The single rule is to stick to simplicity: we don't want an heavy, hard to use,
software ;)

Moreover, it's more a project for study or for small/personal projects, so it's
not aimed to deal with complex situations as a company may encounter.

### Building the project

Same as for installation above.

### Run tests

    node --experimental-vm-modules ./node_modules/.bin/jest

### Run quality check

    ./node_modules/.bin/eslint src/*

